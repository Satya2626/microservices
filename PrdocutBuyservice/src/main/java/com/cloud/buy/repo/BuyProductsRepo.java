package com.cloud.buy.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cloud.buy.model.Products;


@Repository
public interface BuyProductsRepo extends JpaRepository<Products, Integer>{
	
	@Query(value = "select * from Products where modelNo = :modelNo",nativeQuery = true)
	public Products getProductById(@Param ("modelNo") String modelNo);

}
