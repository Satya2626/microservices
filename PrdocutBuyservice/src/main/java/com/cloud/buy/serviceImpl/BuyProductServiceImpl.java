package com.cloud.buy.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloud.buy.model.Products;
import com.cloud.buy.pojo.BuyDetails;
import com.cloud.buy.repo.BuyProductsRepo;
import com.cloud.buy.service.BuyProductService;

@Service
public class BuyProductServiceImpl implements BuyProductService{
	
	@Autowired
	public BuyProductsRepo repo;	

	@Override
	public BuyDetails getProductBuyDetails(String modelNo, Integer quantity) {
		// TODO Auto-generated method stub
		Products products =  repo.getProductById(modelNo);
		
		BuyDetails details = new BuyDetails();
		
		details.setModelNo(modelNo);
		details.setCompany(products.getCompany());
		
		details.setProductName(products.getProductname());
		
		details.setQuantity(quantity);
		
	details.setCost(products.getPrice());
	details.setTotalCost(quantity * products.getPrice());

		
		
		return details;
		
		
		
	}

}
