package com.cloud.buy.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.cloud.buy.pojo.BuyDetails;
import com.cloud.buy.service.BuyProductService;

@RestController
public class BuyController {
	
	
	@Autowired
	public BuyProductService service;
	
	@Autowired
	private Environment environment;
	
	@GetMapping("/getbuydetails/{modelno}/{quantity}")
	public BuyDetails getBuyDetails(@PathVariable ("modelno") String modelno, @PathVariable ("quantity") String quantity) {
		return service.getProductBuyDetails(modelno, Integer.parseInt(quantity));
	}
	@GetMapping("/testport")
	public String testPort() {
		
		
		return "Serving port : "+environment.getProperty("local.server.port");
		
	}
}
