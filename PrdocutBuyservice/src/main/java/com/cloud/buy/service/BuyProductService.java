package com.cloud.buy.service;

import org.springframework.stereotype.Service;

import com.cloud.buy.pojo.BuyDetails;

@Service
public interface BuyProductService {
	
	public BuyDetails getProductBuyDetails(String modelNo, Integer quantity);
	
	

}
