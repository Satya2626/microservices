package com.cloud.product.repo;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cloud.product.model.Products;

@Repository
public interface ProductServiceRepo extends JpaRepository<Products, Integer> {
	
	@Query(value = "Select * from Products",nativeQuery = true)
	
	public List<Products> getAllProducts();

	@Query(value = "select * from Products where modelNo = :modelNo",nativeQuery = true)
	public Products getProductById(@Param ("modelNo") String modelNo);

	/*
	 * @Query(value =
	 * "insert into Products(modelNo,Company,ram,HD,price,description) values(:product.MODELNO,:product.COMPANY,'8GB','1TB',50000,'BusinessLaptop',"
	 * ) public Products addProduct(@Param ("product") Products product);
	 */	
	//public Products saveProducts(Products product);
	

}
