package com.cloud.product.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cloud.product.model.Products;

@Service
public interface ProductService {
	
	public List<Products> getProducts();

	public Products getProductById(String modelNo);

	public Products addProduct(Products product);

}
