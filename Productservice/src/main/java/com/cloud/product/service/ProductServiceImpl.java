package com.cloud.product.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloud.product.model.Products;
import com.cloud.product.repo.ProductServiceRepo;

@Service
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	public ProductServiceRepo repo;

	@Override
	public List<Products> getProducts() {
				return repo.getAllProducts();
	}

	@Override
	public Products getProductById(String modelNo) {
		// TODO Auto-generated method stub
		return repo.getProductById(modelNo);
	}

	@Override
	public Products addProduct(Products product) {
		// TODO Auto-generated method stub
		//return repo.saveProducts(product);
		return repo.save(product);
	}

}
