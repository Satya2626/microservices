package com.cloud.product.controller;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cloud.product.model.Products;
import com.cloud.product.service.ProductService;

@RestController
public class ProductsListController {
	
	
	@Autowired
	private Environment environment;
	@Autowired
	public ProductService service;
	
	@GetMapping("/getallproducts")	
	public List<Products> getAllProducts() {
		return service.getProducts();
	}
	
	@GetMapping("/getproductbymodelno/{modelno}")	
	public Products getProductById(@PathVariable("modelno") String modelNo) {
		System.out.println("call from viewservice");
		return service.getProductById(modelNo);
	}
	
	@GetMapping("/testport")
	public String testPort() {
		
		
		return "Serving port : "+environment.getProperty("local.server.port");
		
	}
	@PostMapping("/addproduct") 
	public Products addProduct(@RequestBody Products product) {
		return service.addProduct(product);
	}
	

}
