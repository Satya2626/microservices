package com.cloud.view.controller;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.cloud.view.model.Products;
import com.cloud.view.service.ProductViewService;

@RestController

public class ProductViewController {
	
	@Autowired
	public ProductViewService service;
	
	@Autowired
	private RestTemplate resttemplate;
	
	final String uri = "http://localhost:9999/productslistservice";
    
	
	@Autowired
	private Environment environment;
	
	@GetMapping("/getproductdetilsbyid/{modelNo}")
	public Products getProductDetailsById(@PathVariable("modelNo") String modelNo) {
		
		
		
		/*
		 * String url = uri+"/getproductbymodelno/"+modelNo+"";
		 * 
		 * 
		 * Products result = resttemplate.getForObject(url, Products.class);
		 * 
		 * System.out.println(result+"=================");
		 */
		//return result;
		
		
		return service.getProductDetailsById(modelNo);
	}
	
	
	
	@GetMapping("/testport")
	public String testPort() {
		
		
		return "Serving port : "+environment.getProperty("local.server.port");
		
	}
	
}
