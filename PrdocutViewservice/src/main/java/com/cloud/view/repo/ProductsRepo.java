package com.cloud.view.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cloud.view.model.Products;

@Repository
public interface ProductsRepo extends JpaRepository<Products, Integer>{
	
	@Query(value = "select * from Products where modelNo = :modelNo",nativeQuery = true)
	public  Products findById(@Param ("modelNo") String modelNo);

}
