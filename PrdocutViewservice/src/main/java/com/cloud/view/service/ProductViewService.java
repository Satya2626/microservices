package com.cloud.view.service;

import org.springframework.stereotype.Service;

import com.cloud.view.model.Products;


@Service
public interface ProductViewService {

	Products getProductDetailsById(String modelNo);

}
