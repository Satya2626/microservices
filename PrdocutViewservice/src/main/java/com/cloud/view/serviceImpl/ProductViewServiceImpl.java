package com.cloud.view.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.cloud.view.model.Products;
import com.cloud.view.repo.ProductsRepo;
import com.cloud.view.service.ProductViewService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class ProductViewServiceImpl implements ProductViewService{

	@Autowired
	public ProductsRepo repo;
	
	@Autowired
	private RestTemplate resttemplate;
	
	final String uri = "http://localhost:9999/productslistservice";
	
	@HystrixCommand(fallbackMethod = "getProductDetailsById_Fallback")
	@Override
	public Products getProductDetailsById(String modelNo) {
		
		
		String url = uri+"/getproductbymodelno/"+modelNo+"";
		
		
		Products result = resttemplate.getForObject(url, Products.class);
		
		System.out.println(result+"=================");
		
		return result;
		
		// TODO Auto-generated method stub
		//return repo.findById(modelNo);
	}
	
	
	@SuppressWarnings("unused")
	private Products getProductDetailsById_Fallback(String modelNo) {
		
		System.out.println("Products service is not up and running");
		return null;
	}

}
