package com.cloud.coupon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cloud.coupon.Repos.CouponRepos;
import com.cloud.coupon.model.Coupon;

@RestController
@RequestMapping("/coupon")
public class CouponController {
	@Autowired
	CouponRepos repo;
	
	
	@PostMapping
	public Coupon createCoupon(@RequestBody Coupon coupon) {
		return repo.save(coupon);
	}
		
	@GetMapping(value = "/coupons/{code}")
	public Coupon getCoupon(@PathVariable("code") String code) {
		return repo.findByCode(code);
	}

}
