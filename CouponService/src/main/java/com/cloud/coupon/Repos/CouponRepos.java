package com.cloud.coupon.Repos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cloud.coupon.model.Coupon;

public interface CouponRepos extends JpaRepository<Coupon, Long> {

	Coupon findByCode(String code);

}
